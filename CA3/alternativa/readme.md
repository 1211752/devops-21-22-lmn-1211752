>> # DEVOPS

> ## CA3 (class assignment 3)


>### Alternativa

Virtualização - Criar dois VM's recorrendo ao software Vagrant recorrendo ao vmware.

>**Passo 1.1** - Criar pasta CA3/part2 e readme.md

Abrir o terminal/linha de comandos da máquina local e mudar para o diretório pretendido:

`§ cd devops-21-22-lmn-1211769/CA3` - Este comando dá acesso ao diretório pretendido.

`§ mkdir alternativa`- Para criar a pasta com o propósito de guardar os documentos referentes
à resolução deste exercício

`§ nano readme.md` - para criar o ficheiro readme


>**Passo 1.2** - Fazer cópia do vagrantfile utilizado no CA3-part2

Fazer uma cópia do vagrantfile utilizado no CA3-part2 para a pasta CA3/alternativa


>**Passo 1.3** - Instalar o vm ware fusion

https://www.vmware.com/products/fusion/fusion-evaluation.html

>**Passo 1.4** - Instalar  o vagrant vmware
 
Fazer download em :  https://www.vagrantup.com/vmware/downloads

Executar o comando:
`§ vagrant plugin install vagrant-vmware-desktop` 

>**Passo 1.5** - Alterar as configurações no vagrantfile

- alterar o provider:

  - web.vm.provider "vmware_fusion" do |v|

- retirar as referências aos IP's, pois a versão Mac BigSur não permite definir os ip's 
desta forma.


>**Passo 1.6** - correr o ficheiro vagrantfile

``§ vagrant up``

>**Passo 1.7** - Definir os Ips das duas VM criadas

Seguindo os passos presentes em 

https://docs.vmware.com/en/VMware-Fusion/12/fusion-12-user-guide.pdf


Criam-se os dois IPs necessários através da definição de uma rede virtual vmnet3:


![](ip_db.png)


![](ip_web.png)


Posteriormente é necessário fazer a alteração no application.properties
e colocar o ip da db:

spring.datasource.url=jdbc:h2:tcp://192.168.180.131:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE


Posteriormente desligam-se as VM

``§ vagrant halt db ``

``§ vagrant halt web ``

Para correr as novas alterações às VM previamente criadas, executar o comando:

``§ vagrant up --provision ``

Seria de esperar que ao correr os URL, os mesmos funcionassem mas tal não aconteceu.


http://192.168.180.129:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/

http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console

De qualquer das formas, este foi um trabalho exaustivo que permitiu perceber que há muitas
nuances envolvidas com a criação de vm através de Vagrant e que os softwares de virtualização
diferem uns dos outros.



>**Passo 1.8** - VirtualBox vs VmWare

- O VirtualBox pode ser instalado em máquinas host Linux (i.a. Ubuntu e Debian), Windows, Solaris, macOS e FreeBSD. 
E é possível escolher Linux, Windows, Solaris, FreeBSD ou macOS como sistema operacional guest na máquina virtual criada.

- O VMware Workstation e Player têm versões para Linux e Windows. Para macOS existe a versão Fusion. 
Como sistema operacional guest, tem as seguintes opções: Linux, Windows, Solaris, FreeBSD ou macOS.

- Ao contrário do VMware, o VirtualBox é opensource tornando, invariavelmente, mais utilizado pela comunidade global.

- O VirtualBox permite virtualizar tanto hardware como software. 

- O VMware só permite virtualizar hardware.

- O VirtualBox suporta VDI (Virtual Disk Image), Virtual Machine Disk (VMDK) e Virtual Hard Disk (VHD).

- O VMware apenas suporta Virtual Machine (VMDK).

- A nível de interfaces, o VirtualBox possui uma user-friendly interface e por oposição, o VMware apresenta
uma interface mais complexa.

- O VirtualBox apenas tem capacidade de 128 MB (para memória de video).

- O VMware tem capacidade até 2 GB.

- O VirtualBox tem a funcionalidade do snapshots, permitindo ao utilizador salvar e restaurar, caso necessite, a sua máquina virtual.

- O VMware, sendo um software pago só permite esta funcionalidade, pagando pelo serviço.



>**Passo 1.9.** - Commit das alterações efetuadas


`§ git add .`

`§ git commit -m "updating readme file"`

`§ git push`

`§ git tag ca3-part2-alternativa`

`§ git push origin ca3-part2-alternativa`


