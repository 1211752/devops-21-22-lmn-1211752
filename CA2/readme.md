# Git Tutorial

## Part I
#### Step 1 - Create new directory (CA2):
* `§ mkdir CA2/`

#### Step 2 - Change to the new directory:
* `§ cd CA2`

#### Step 3 - Create a new readme.md file and add to repository:
* `§ touch readme.md`
* `§ git add readme.md`
* `§ git commit -m ‘Implementation: create new directory CA2 and readme.md fil [resolves #14; resolves #15]’`
* `§ git push`

#### Step 4 - Create new directory (CA2-partI):
* `§ mkdir CA2-partI/`

#### Step 5 - Change to the new directory:
* `§ cd CA2-partI`

#### Step 6 - Clone repository to the file CA2-partI
* `§ git clone https://csptcunha@bitbucket.org/luisnogueira/gradle_basic_demo.git`

#### Step 7 - Remove .git file
* `§ rm -rf .git`

#### Step 8 - Commit changes
* `§ git add .`
* `§ git commit -m ‘Implementation: create new directory CA2-partI and cloned gradle repository into new directory [resolves #16; resolves #17]’`
* `§ git push`

#### Step 9 - Tag the last pushed commit with its version:
* `§ git tag -a v2.1.0 -m 'first version tag'`
* `§ git push origin v2.1.0`

#### Step 10 - Open gradle_basic_demo to check instructions
* At this step I've opened the readme.md file in intellij to read the instructions for the first experiment on the aplication.

#### Step 11 - Build a project in gradle
* `§ ./gradlew build`

#### Step 12 - Run the server
* `§ java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001`

#### Step 13 - Open new terminal in new directory CA2-partI and run the following command to execute chat
* `§ ./gradlew runClient`

#### Step 14 - Create new task to run the server and substitute previous extensive commmand of step 11
* At this step I opened build.gradle on Intellij, and created a new task to run the server as following:

> task runServer (type:JavaExec, dependsOn: classes) {
>    
>   group = "DevOps"
>   description = "Launches a server chat client that connects to a server on localhost:59001"
>   classpath = sourceSets.main.runtimeClasspath
>   mainClass = 'basic_demo.ChatServerApp'
>   args 'localhost', '59001'
> 
> }

#### Step 15 - Build a project in gradle
* `§ ./gradlew build`

#### Step 16 - Run the server using the new task
* `§ ./gradlew runServer`

#### Step 17 - Open new terminal in new directory CA2-partI and run the following command to execute chat
* `§ ./gradlew runClient`

#### Step 18 - For ex4 of CA2 it's requested to create a new simple unit test and update script to run test
* At this step I opened build.gradle and updated the dependencies fied with junit as:
  testImplementation 'junit:junit:4.12'
* Then I've created a new class AppTest and made a test and ran it successfully.

#### Step 19 - For ex5 it is asked to create a new task that copies the content from src folder into a new backup folder
* At this step I redacted a new task called copyFromSource in build.gradle as following:

> task copyFromSource (type: Copy) {
>
>     from 'src/'
>     into 'backup/'
>
>}

* This task copies all content inside the src folder and pastes its content inside a new backup folder (generated automatically
since it didn't exist)

#### Step 20 - For ex5 we are asked to create a task to create a zip archive of src file and copy its content to a zip file
* At this step I redacted a new task called zipFile in build.gradle as following:
  
> task zipFile (type: Zip) {
>
>     archiveFileName = 'src.zip'
>     from 'src/'
>     destinationDirectory = file('backup')
> 
>}

#### Step 21 - Tag the last commit to seal the project
* `§ git tag -a ca2-part1 -m 'final tag part1'`
* `§ git push origin ca2-part1`

<br>

## Part II
#### Step 1 - Create new directory CA2-partII:
* `§ mkdir CA2-partII/`

#### Step 2 - Create new branch in my CA2 repository:
* `§ git branch tut-basic-gradle`

#### Step 3 - Switch the head to the new branch
* `§ git checkout tut-basic-gradle`

#### Step 4 - Change to the new directory:
* `§ cd CA2-partII`

#### Step 5 - Build gradle inside my new repository:
* `§ ./gradlew build`

#### Step 6 - Check the available gradle tasks:
* `§ ./gradlew tasks`

#### Step 7 - Delete src file:
* `§ rm -rf src`

#### Step 8 - Copy src directory from CA1/tut-react(...)/basic/src into CA2-partII/tut-react(...):
* `§ cp -R CA1/tut-react-and-spring-data-rest-main/basic/src CA2/CA2-partII/react-and-spring-data-rest-basic/`

#### Step 9 - Copy webpack.config.js and package.json too:
* `§ cp -R CA1/tut-react-and-spring-data-rest-main/basic/webpack.config.js CA2/CA2-partII/react-and-spring-data-rest-basic/`
* `§ cp -R CA1/tut-react-and-spring-data-rest-main/basic/package.json CA2/CA2-partII/react-and-spring-data-rest-basic/`

#### Step 10 - Remove folder src/main/resources/static/built/:
* `§ cd (...) static`
* `§ rm -rf built`

#### Step 11 - Run the frontend application:
* `§ ./ gradlew bootRun`

#### Step 12 - Create new task to copy the generated jar to a folder named "dist" located athe project root folder level:

> task copyJar(type: Copy) {
> 
>  from 'build/libs/*.jar'
> 
>  into 'dist'
>  
>  }

#### Step 13 - Create new task to delete all the files generated by webpack and should be executed automatically by gradle before the task clean.:

> clean.doFirst {
> 
> delete "src/main/resources/static/built"
> 
> }

#### Step 14 - Commit changes and merge into the Main branch
* `§ git commit -a -m 'final commit'`
* `§ git push origin tut-basic-gradle`
* `§ git checkout Main`
* `§ git merge tut-basic-gradle`

#### Step 15 - Tag the last commit to seal the project
* `§git tag ca2-part2`
* `§git push origin ca2-part2`