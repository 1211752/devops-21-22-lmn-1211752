# Git Tutorial

## Part I - Create directory (CA1) and copy existing file inside the new directory
#### Step 1 - Setup user profile, to identify commits made:
* git config --global user.name "Carlos Cunha [1211752]”
* git config --global user.email 1211752@isep.ipp.pt

#### Step 2 - Setup text editor:
* git config --global core.editor nano
* git config -l

#### Step 3 - Create new directory (CA1):
* mkdir CA1/

#### Step 4 - Change to the new directory:
* cd CA1

#### Step 5 - Create a new readme.md file and add to repository:
* touch readme.md
* git add readme.md
* git commit -m ‘initial project version’
* gti push

#### Step 6 - Copy all internal files of tut-react-and-spring-data-rest into CA1:
* cp -R tut-react-spring-data-rest CA1/

#### Step 7 - Tag the last pushed commit with its version:
* git tag -a v1.1.0 -m 'first version tag' 
* git push origin v1.1.0

#### Step 8 - Tag the last commit to seal the project
* git tag -a ca1-part1 'final tag part1'
* git push origin ca1-part1t

<br>

## Part II - Create new branches and merge the new features to master branch
#### Step 1 - Create and name a new branch
* git branch testBranch

#### Step 2 - Switch the head to the new branch
* git checkout testBranch

#### Step 3 - Make changes on new branch
* git commit -a -m  'made new changes'
* git push origin email-field

#### Step 4 - Check the new branch and master branch commits/issues "graphically"
* git log --oneline --decorate --graph --all

#### Step 5 - Create an issue, let's say #53, branch
* git branch iss53
* git checkout iss53
* nano index.html
* git commit -a -m 'added a new footer [issue 53]'

#### Step 6 - Create a hotfix branch to resolve a bug on website
* git checkout master
* git checkout -b hotfix 
* nano index.html
* git commit -a -m 'fixed the broken email address'

#### Step 7 - Merge the hotfix branch to the master branch after being tested
* git checkout master
* git merge hotfix

#### Step 8 - Delete the hotfix branch, since it's no longer needed
* git branch -d hotfix

#### Step 9 - Get back to work on issue 53
* git checkout iss53 
* nano index.html
* git commit -a -m 'finished the new footer[issue 53]'
* git push origin iss53

#### Step 10 - When finished, we want to merge it back to master branch
* git checkout master
* git merge iss53

#### Step 11 - Sometimes a merge has conflicted changes that has to be resolved manually
* git status
* nano index.html
* (Choose which parts I want to keep and delete the rest)
* git add
* git commit (when happy with final changes)

<br>

## Alternative
* Used Software - Mercurial
* Used Repository - SourceForge

#### Step 1 - Clone repository
* hg clone ssh://ccunha@hg.code.sf.net/p/devops-21-22-lmn-1211752/devops-21-22-lmn-1211752 devops-21-22-lmn-1211752-devops-21-22-lmn-1211752

#### Step 2 - Create readme file
* touch readme

#### Step 3 - Add changes to commit 
* hg add

#### Step 4 - Configurate user profile to associate to commits
* hg config --edit 'Carlos Cunha [1211752]'

#### Step 5 - Commit changes made
* hg commit -m 'Initial commit'

#### Step 6 - Push commit made
* hg push

#### Step 7 - Create a version tag to the last commit made
* hg tag v1.1.0 'initial tag'

#### Step 8 - Push tag
*hg push

#### Step 9 - Create a new branch
* hg branch email-field
* hg commit -m 'created new branch'
* hg push

#### Step 10 - Change head pointer to default branch 
* hg up default

#### Step 11 - Made changes to readme file to create conflict
* hg add
* hg commit -m 'made changes to readme file'
* hg push

#### Step 12 - Change head pointer to emaild-field branch
* hg up email-field
* hg add
* hg commit -m 'new changes made'
* hg push

#### Step 13 - Get back to default branch
* hg up default
* hg merge email-field

At this step appeared vim editor with the changes to make to resolve the conflicted changes.
With the key "i" to edit file and after the edition ":wq" to save and exit the editor.

* hg add
* hg commit -m 'commit the merged code'
* hg push 

#### Step 14 - Create tag version
* hg tag v1.3.0
* hg push

#### Step 15 - Create a new branch fix-invalid-email
* hg branch fix-invalid-email
* hg commit -m 'created new branch fix-invalid-email'
* hg push

#### Step 16 - Change head pointer to default branch
* hg up default

#### Step 17 - Made changes to employee email field file to create conflict
* hg add
* hg commit -m 'made changes to emplyee claas'
* hg push

#### Step 18 - Change head pointer to emaild-field branch
* hg up fix-invalid-email
* hg add
* hg commit -m 'new changes made to email field'
* hg push

#### Step 19 - Get back to default branch
* hg up default
* hg merge fix-invalid-email

At this step appeared vim editor with the changes to make to resolve the conflicted changes.
With the key "i" to edit file and after the edition ":wq" to save and exit the editor.

* hg add
* hg commit -m 'commit the merged code'
* hg push

#### Step 20 - Create tag version
* hg tag ca1-part2
* hg push

#### Repository Link:
* https://sourceforge.net/p/devops-21-22-lmn-1211752/devops-21-22-lmn-1211752/ref/default/

### Pros and Cons of the alternative Mercurial
Mercurial has a lot of similarities with git. They are both distributed version control systems, and they share some
commands such as:
* **config** - command to configurate the user email and name to assign to the commit;
* **add** - command to add the last changes to the commit path;
* **commit** - command that submits the added changes in the add command;
* **push** - command that pushes the commit made to the server;
* **branch** - command that generates a new branch;
* **tag** - command that assigns to the last commit made a tag;
* **merge** - command that merges the changes made in the branch created to the main branch.

The different commands where very little. Here are some examples of those differencess:
* **up** - change the head pointer between branches instead of the chekout command in git;
* **branches** - command to see the existing branches and in which is the head pointer instead of the branch 
(without branch name in front) in git;
* The head pointer in mercurial is green bold on the branch name instead of the "*" behind the branch name in git.

The remote repository that I used was SourceForge.net as an alternative to the bitbucket. On the other side, I used 
intellij as GUI, since I wasn't able to work with TortoiseHg (and I couldn't find why it wasn't working). The commands 
where all executed on the macOS terminal.
For effects of the assignment I manually copied the tut-react folder used for CA1 with the changes and fields added, and
made changes to the file to simulate the intended objectives of the assignment to create and generate conflicts between
the main (default) branch and the new branches created on the merge fase.

Some cons I've found about mercurial was the lack of commands compared to git. Git is much more complete, such as deleting
the unsused branches that mercurial isn't able to do. 

<br>

## General Commands:

#### List all documents (hidden(the ones that has a dot at beginning) and the ones not hidden)
* ls -la;

#### List not hidden documents
* ls -l;

#### List git configurations 
* git config -l;

#### Listing of your current branches
* git branch (the * prefix means that branch is the one that head points to)

#### List last commit on each branch
* git branch -v

#### List the branches that are already merged into the branch you’re on
* git branch --merged

#### To synchronize your work
* git fetch origin