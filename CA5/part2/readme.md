
# CI/CD Pipelines with Jenkins

## Part II
#### Step 1 - Copy gradle_basic_demo into this directory
* Copied the folder from CA2/CA2-partI into my folder CA5/part2 to simplify the changes to be made.
* obs.: this folder has the changes made on CA5 part1. 

#### Step 2 - Add to Jenkinsfile -> Javadoc

`stage('Javadoc') { 
    steps {
        echo 'Javadoc...'
            dir('CA5/part2/gradle_basic_demo') {
            sh './gradlew javadoc'
        }
        publishHTML([allowMissing: false,
            alwaysLinkToLastBuild: false,
            keepAll: false, reportDir: 'CA5/part2/gradle_basic_demo/build/docs/javadoc',
            reportFiles: 'index.html',
            reportName: 'HTML Report',
            reportTitles: 'The Report'])
        }
    }
}`

#### Step 3 - Add to Jenkinsfile -> Docker Image
`
stage('Docker Image'){
    steps {
        script {
            def customImage = docker.build("my-image:${env.BUILD_ID}")
            customImage.push()
        }
    }
}
`

#### Step 4 - Create Dockerfile
* On intellij created Dockerfile to source root of directory

#### Step 5 - Edited Dockerfile
`
Base image ubuntu 18.04
FROM ubuntu:18.04

Disable Prompt During Packages Installation
ARG DEBIAN_FRONTEND=noninteractive

Install java
RUN apt-get update -y
RUN apt-get install openjdk-11-jdk-headless -y

Copy the jar file into the container
COPY build/libs/basic_demo-0.1.0.jar .

Expose Port for the Application
EXPOSE 59001

Run the server
CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

#### Step 6 - Commit final changes
* `§ git commit -a -m 'final commit'`
* `§ git push`

#### Step 7 - Tag the last commit to seal the project
* `§ git tag ca5-part2`
* `§ git push origin ca5-part2`